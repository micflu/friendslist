import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Feather } from '@expo/vector-icons';

import HomeScreen from './screens/HomeScreen';
import SettingsScreen from './screens/SettingsScreen';
import FriendScreen from './screens/FriendScreen';


const HomeStack = createStackNavigator();
const TabNavigator = createBottomTabNavigator();

export default class AppNavigator extends Component {

    HomeStackScreen = () =>
        <HomeStack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: 'aliceblue',
                },
            }}
        >
            <HomeStack.Screen
                name="Home"
                component={HomeScreen}
            />
            <HomeStack.Screen
                name="FriendScreen"
                component={FriendScreen}
                options={({ route }) => ({
                    headerTitleStyle: { //alignSelf: 'center' 
                    },
                    headerStyle: {
                        backgroundColor: 'aliceblue',
                    },
                })}

            />
        </HomeStack.Navigator>

    render() {
        return (
            <NavigationContainer>
                <TabNavigator.Navigator
                    initialRouteName="Home"
                    tabBarOptions={{
                        activeTintColor: 'darkorange',
                        activeBackgroundColor: 'aliceblue',
                        inactiveBackgroundColor: 'aliceblue',
                    }}
                >
                    <TabNavigator.Screen
                        name="Home"
                        component={this.HomeStackScreen}
                        options={{
                            tabBarLabel: 'Home',
                            tabBarIcon: ({ color, size }) => (
                                <Feather name="home" size={size} color={color} />
                            ),
                        }}
                    />
                    <TabNavigator.Screen
                        name="Settings"
                        component={SettingsScreen}
                        options={{
                            tabBarLabel: 'Settings',
                            tabBarIcon: ({ color, size }) => (
                                <Feather name="settings" size={size} color={color} />
                            ),
                        }}
                    />
                </TabNavigator.Navigator>
            </NavigationContainer>
        );
    }
}
