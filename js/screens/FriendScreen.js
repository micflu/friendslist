import React, { Component } from 'react';
import { Dimensions, StyleSheet, Text, View, Image, ScrollView } from 'react-native';


export default class FriendScreen extends Component {
    render() {
        const { friend } = this.props.route.params;
        this.props.navigation.setOptions({ title: `${friend.name.first} ${friend.name.last}` });
        return (
            <ScrollView
                style={styles.scrollview}
                contentContainerStyle={styles.container}
            >
                <Image
                    style={styles.image}
                    source={{ uri: friend.picture.large }} />
                <Text>{friend.name.first} {friend.name.last}</Text>
            </ScrollView >
        );
    };
}

const width = Dimensions.get('window').width * 0.75;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    scrollview: {
        backgroundColor: '#fff'
    },
    image: {
        width: width,
        height: width,
        marginRight: 20,
        marginTop: 20
    },
});